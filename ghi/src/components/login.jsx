import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { useState } from 'react'
import { login } from '../services/auth'
import { useNavigate } from 'react-router-dom'
import ocean_background from '../images/ocean_background.png'

const LoginForm = () => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const { setToken, baseUrl } = useAuthContext()
    const navigate = useNavigate()

    const handleSubmit = async (e) => {
        e.preventDefault()
        const form = e.currentTarget
        try {
            const token = await login(baseUrl, username, password)
            setToken(token)
            navigate('/explore')

        } catch (e) {
            if (e instanceof Error) {
                setError(e.message)
                form.reset()
            }
        }

    }

    return (
        <div
            className="bg-fixed bg-cover bg-center h-screen pt-40 bg-blue-200  "
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <div className="p-8 bg-white max-w-sm mx-auto my-10 border border-slate-400 rounded-lg shadow-lg">
                <h5 className="text-center text-2xl font-bold mb-4">Login</h5>
                {error ? <p className="text-red-500 mb-4">{error}</p> : null}
                <div>
                    <form onSubmit={(e) => handleSubmit(e)}>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                                Username:
                            </label>
                            <input
                                name="username"
                                type="text"
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={(e) => setUsername(e.target.value)}
                            />
                        </div>
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                                Password:
                            </label>
                            <input
                                name="password"
                                type="password"
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>
                        <div className="justify-center items-center text-center">
                            <button
                                className="bg-slate-900 text-red-400 font-bold px-4 py-2 rounded shadow-sm shadow-slate-900 border border-slate-500 hover:bg-slate-500"
                                type="submit"
                            >
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LoginForm

import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
const API_HOST = import.meta.env.VITE_API_HOST
import ocean_background from '../images/ocean_background.png'

function Signup() {
    const navigate = useNavigate()
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        email: '',
        username: '',
        password: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault()
        const fetchConfig = {
            method: 'post',
            credentials: 'include',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const url = `${API_HOST}/api/accounts`

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            event.target.reset()
            navigate('/explore')
            window.location.reload()
        } else {
            const responseJson = await response.json()
            alert(responseJson.detail)
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({ ...formData, [inputName]: value })
    }

    return (
        <div
            className="pt-32 bg-fixed bg-cover bg-center min-h-screen"
            style={{ backgroundImage: `url(${ocean_background})` }}
        >
            <div className="p-8 bg-white max-w-sm mx-auto my-10 border border-slate-400 rounded-lg shadow-lg">
                <div className="row">
                    <div className="text-center text-2xl font-bold mb-4">
                        <p>Signup</p>
                    </div>
                    <form onSubmit={handleSubmit} id="login-form">
                        <div className="form-floating mb-3">
                            <label htmlFor="first_name: ">First Name</label>
                            <input
                                name="first_name"
                                type="text"
                                required
                                value={formData.first_name}
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={handleFormChange}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="last_name: ">Last Name </label>
                            <input
                                name="last_name"
                                type="text"
                                required
                                value={formData.last_name}
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={handleFormChange}
                            />
                        </div>

                        <div className="form-floating mb-3">
                            <label htmlFor="email: ">Email </label>
                            <input
                                name="email"
                                type="text"
                                required
                                value={formData.email}
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={handleFormChange}
                            />
                        </div>

                        <div className="form-floating mb-3">
                            <label htmlFor="username ">Username</label>
                            <input
                                name="username"
                                type="text"
                                required
                                value={formData.username}
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={handleFormChange}
                            />
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="password: ">Password</label>
                            <input
                                name="password"
                                type="password"
                                required
                                value={formData.password}
                                className="border-2 border-gray-300 p-2 w-full rounded"
                                onChange={handleFormChange}
                            />
                        </div>
                        <div className="justify-center items-center text-center">
                            <button
                                type="submit"
                                className="bg-slate-900 text-red-400 font-bold px-4 py-2 rounded shadow-sm shadow-slate-900 border border-slate-500 hover:bg-slate-500"
                            >
                                Create Account
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default Signup

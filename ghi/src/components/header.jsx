import { useEffect, useState, useRef } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { logout } from '../services/auth'
import profile_pic2 from '../images/profile_pic2.png'
import prawnto_logo from '../images/prawnto_logo.png'

const API_URL = import.meta.env.VITE_API_HOST

function Header() {
    const navigate = useNavigate()
    const { token } = useToken()
    const [show, setShow] = useState(false)
    const [loggedIn, setLoggedIn] = useState(false)
    const menuRef = useRef()
    const imgRef = useRef()

    window.addEventListener('click', (e) => {
        if (e.target !== menuRef.current && e.target !== imgRef.current) {
            setShow(false)
        }
    })

    const handleClick = () => {
        setShow(!show)
        setLoggedIn(token)
        logout(API_URL)
        navigate('/login')
        window.location.reload()
    }

    const loginStatus = async () => {
        setLoggedIn(token)
    }

    useEffect(() => {
        loginStatus()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])

    return (
        <div>
            <div className="relative z-10">
                <div className="flex fixed top-0 left-0 h-25 w-full shadow-lg bg-slate-900 justify-between items-center">
                    <Link to="/" className="active">
                        <div className="flex items-center justify-center pl-6">
                            <img
                                className="h-32"
                                src={prawnto_logo}
                                alt="logo"
                            />

                            <h1 className="text-5xl text-red-500 font-bold mx-auto hidden lg:block">
                                Prawnto
                            </h1>
                        </div>
                    </Link>
                    <div>
                        <ul className="flex text-red-500 font-medium space-x-6 text-left pr-6 text-xl">
                            {!loggedIn ? (
                                <li className="px-4 py-2 hover:bg-slate-700 hover:cursor-pointer border border-red-500 rounded-lg">
                                    <Link to="/login" className="active">
                                        Login
                                    </Link>
                                </li>
                            ) : null}
                            {!loggedIn ? (
                                <li className="px-4 py-2 hover:bg-slate-700 hover:cursor-pointer border border-red-500 rounded-lg">
                                    <Link to="/signup" className="active">
                                        Signup
                                    </Link>
                                </li>
                            ) : null}
                            {loggedIn ? (
                                <li
                                    onClick={() => setShow(!show)}
                                    className="hover:bg-red-500 hover:rounded-full p-1 hover:cursor-pointer"
                                >
                                    <img
                                        ref={imgRef}
                                        className="w-20 h-20 border-2 border-slate-900 rounded-full"
                                        src={profile_pic2}
                                    />
                                </li>
                            ) : null}
                        </ul>
                    </div>
                </div>
            </div>
            {/* Dropdown menu */}
            {show ? (
                <div
                    ref={menuRef}
                    className="fixed z-20 top-20 right-20 bg-white rounded-lg shadow-lg h-auto w-44 border border-slate-500"
                >
                    <ul>
                        <div className="w-full hover:bg-slate-900 hover:text-red-400 font-medium px-3 items-center justify-center text-center">
                            <Link to="/manage" className="active">
                                <li
                                    onClick={() => setShow(!show)}
                                    className="mt-2"
                                >
                                    Manage
                                </li>
                            </Link>
                        </div>
                        <Link to="/explore" className="active">
                            <div
                                onClick={() => setShow(!show)}
                                className="w-full hover:bg-slate-900 hover:text-red-400 font-medium px-3 items-center justify-center text-center"
                            >
                                <li>Explore</li>
                            </div>
                        </Link>

                        <Link to="/attemptedexam" className="active">
                            <div
                                onClick={() => setShow(!show)}
                                className="w-full hover:bg-slate-900 hover:text-red-400 font-medium px-3 items-center justify-center text-center"
                            >
                                <li>My Attempts</li>
                            </div>
                        </Link>

                        <div
                            onClick={handleClick}
                            className="w-full hover:bg-slate-900 hover:text-red-400 font-medium px-3 items-center justify-center text-center hover:cursor-pointer border-t border-slate-500 my-3 py-2"
                        >
                            <li>Logout</li>
                        </div>
                    </ul>
                </div>
            ) : null}
        </div>
    )
}
export default Header

<center>

<img src="ghi/src/images/prawnto_logo.png" width="200">

# PRAWNTO

</center>

## **About The Project**

Our FastAPI application is an education tool that facilitates CRUD operations.

## **Intended Market**

Our initial intended market includes anyone that is looking to create an exam to verify understanding of a topic, no matter the scale. We welcome the church pastor who needs to verify someone's understanding of lighting and sound for a church service, and we welcome a fortune 500 company looking to have their thousands of employees take a compliance certification. Our other initial target market is for anyone looking to gain knowledge and get confirmation of their understanding of said information. This could allow people to show proof of their capabilities to their current or future employers. Our future target market includes schools of all sizes: elementary to postgrad. The future of ***PRAWNTO*** is a full service education platform with not only exams, but educational materials, student to teacher communication, student to student communication, and the ability to show student improvement over time, allowing any educator to feel more confident in their teaching.

## **User Stories**

### Features:

A user can sign up, login and is directed to the explore page that displays all available exams.

A user can create an exam by navigating to the "manage page" via dropdown menu, on the top right of the page their is a make exam button.

A user can enter exam title and exam description on the "make exam page." When a user selects the create exam button on the page the user is directed to the "make exam questions page."

A user can enter inputs for question texts and provide four answer options. Then a user can select the correct answer, via the toggle button next to the selected options.

After entering the prompted data, a user can select two buttons to choose from, create question or submit exam. When a user selects the create question button, it will prompt the user for another question and options. Once the user has entered all their questions, then they select submit exam and are directed back to "my created exams page."

A user can view all exams by user on the "created exams page" and they can select view gradebook for the particular exam. Then, that directs the exam creator to view all exam attempts that displays username, students score, and attempt number.

Users can use the dropdown feature to select explore, taking them back to welcome page that displays all available exams. Then a user can select the exam of their choice and press the take exam button that directs a user to the "take exam" page where multiple choice question and answers are displayed. After a User answers the exam questions they select submit exam and are directed to the "attempted exams page" that shows all exams the user has attempted.

## **Tech Stack**

The tech stack for ***PRAWNTO*** includes:
- FastAPI and Python for the back-end
- PostgreSQL for the database tables
- VITE/React for front-end management
- JavaScript for front-end logic
- Tailwind for making the front-end CSS pretty
- Docker for containerization
- Gitlab used for deployment and CI/CD
- Cirrus used for server deployment

## **Onboarding**

**1.** Fork and clone the repository at https://gitlab.com/mantis-shrimp1/Prawnto.git

**2.** CD into the new project directory

**3.** Open up VS code using `code .` command in your terminal and create a .env file at the project layer and input the following keys into the file


    SIGNING_KEY = {generate a random 32hex signing key using command openssl rand -hex 32 in your terminal}

    CORS_HOST = {enter your cors host url}

    POSTGRES_PASSWORD = {enter your password}

    POSTGRES_USER = {enter your username}

    DATABASE_URL = {enter your postgres database url}

    PGADMIN_DEFAULT_EMAIL = {enter your email}

    PGADMIN_DEFAULT_PASSWORD = {enter your password}

    VITE_API_HOST = {enter your vite api host url}

    REACT_APP_USER_SERVICE_API_HOST = {enter your react api host url}

    BASE_URL = ""

    VITE_PUBLIC_URL = {enter your vite public url}


**4.** Open Docker and run the following commands into your terminal


    Docker volume create pg-admin

    Docker volume create fastapi-data

    Docker compose build

    Docker compose up


**5.** Ensure your docker is running these containers ***db*, *fastapi*, *ghi*, *admin***

**6.** View project

- [VITE front-end](http://localhost:5173/)
- [Project database in pg-admin](http://localhost:8082/login?next=%2Fbrowser%2F)
- [API endpoints swagger](http://localhost:8000/docs#/)

**7.** Enjoy using ***PRAWNTO!***

## **Unit Tests**

Open the fastapi docker container and run the command `pip install flake8`. After installed run command `flake8.` to initiate the linter to check for any errors. If errors are found, correct them in your code. When there are no remaining errors you can run all the unit tests by running the command `pytest`

- [Unit Tests by Jeremy Kilcrease](api/tests/test_exam_by_attempt_id.py)

- [Unit Tests by Jim Floyd](api/tests/test_exam.py)

- [Unit Tests by Denver Alexander](api/tests/test_get_question_by_exam.py)

- [Unit Tests by Sam Madvig](api/tests/test_options_by_question_id.py)

## Documentation

- [DataBase Schema](docs/QuickDataBaseDiagram-Prawnto.png)
- [Wireframe](docs/wireframe.md)

## **Deliverables**

-   [x] [Wire-frame diagrams](docs/wireframe.md)
-   [x] [API documentation](endpoints.txt)
-   [x] Project is deployed to Cirrus (BE, DB) & GitLab-pages (FE)
-   [x] [Jira board is setup and in use](docs/Jiras%20-%20Project%20Tracking)
-   [x] [Journals](journals)

## **Issue Tracking**

## **Team Member Journals**

- [Denver Alexander](journals/Denver_Alexander.MD)</br>
- [Jeremy Kilcrease](journals/Jeremy_Kilcrease.MD)</br>
- [Jim Floyd](journals/Jim_floyd.md)</br>
- [Sam Madvig](journals/Sam_Madvig.md)

## **GitLab pages URL**

- [PRAWNTO](https://prawnto-mantis-shrimp1-7ce913ce20e288a2cb4533210c81ea6e720e3afc.gitlab.io/)

## **Stretch Goals**

We think that this project could be expanded quite easily into a more full fledged education platform. To that end, we believe that future improvements to the platform could include:

- The addition of classes to the platform, allowing there to be a class owner, and students could belong to the class.
- Exams could be made available to students based off the class to which they belong.
- We could add the capacity to store and show profile pictures.
- We could add the capacity to store and show pictures associated with exams and/or classes.
- We could add the ability to allow teachers to present learning materials to students in their classes, like links to required readings, flash cards, copies of class powerpoints, etc.
- We could add more robust views into exam attempts, showing the student the attempt number for that specific exam for that student, instead of the existing exam_attempt_id that is displayed.
- We could show the average grade of an exam to the teacher to provide insights into where they could spend additional time teaching.
- We could provide even more granular views into success rates based on each question, allowing the teacher to know where students may be struggling.
- Provide notifications to users when new exams are created, so they can take them ***PRAWNTO***
- Connect to ChapGPT API to have it grade essay questions for students.
- Add the capacity to have different point values for different exam questions.
- Add the ability to have questions with multiple checkboxes required for being a correct answer.
- Add the ability to increase or decrease the number of questions on the create question page.
- Create a page to show certificates of passed exams for proof for employers.
- Create student message boards

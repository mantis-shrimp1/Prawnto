Jan 16th, 2024
Team worked together to set up docker and database
No blockers

Jan 17, 2024
Team started working on authenticator for log-in and log-out. Currently we're working on creating functions for getting account and creating account that require tokens for our endpoints. Get account is working, however still working on create acct.

Jan 18, 2024
Sam discussed changes made to create acct function to get it working. Completed authentication functions. Afterwards each team member drove creating tables for database & merged. Working on putting together the API's now.

Jan 19, 2024
Created view_all_exam_attempts and get_exam_by_creator endpoints. Worked together with team taking turns driving.


Jan 22, 2024
Everyone took turns driving and completing their API endpoints. I finished create_question_responses. Lead when creating PG admin. Worked with the team when creating the .env file to protect sensitive info.

Jan 23, 2024
We finished the API endpoints. I partnered with Denver to figure out how to use tailwind. We also had trouble logging in and realized it was a result of the authenticator code we inserted in each router, i committed those out for now so we could still log in for the time-being. We also troubleshooted an issue with the team since my localhost:5173 wasn't populating, this was resolved.

Jan 24, 2024
Sam and I partnered to put together the sign-up page. I drove while sam shadowed. We experienced a blocker with the authorization token. We realized through some hours of the entire team troubleshooting this issue was we forgot to include the credentials code that is required for authorized users.

Jan 25, 2024
We worked as a team to overcome token blockers dealing with the sign-in page. We experienced multiple issues in logging in a user and having the tokens follow the particular user logged in throughout multiple webpages. We realized our code for login form did not include the set token call and our baseUrl was incomplete resulting in this blocker. This was resolved and we decided to start working on the front end, putting together the navigation links and creating blank pages for us to later populate with data. Denver and I partnered, I drove while he shadowed and we completed the make exam page and included the routers/imports for navigation.

Jan 29, 2024
We started together mob programming trying to troubleshoot the issue of not populating exam name in the exam page. With SEIR help we realized we need a const function to call the exam name and we forgot to pass the dependent variable in the use effect when fetching. Afterwards I drove while creating the blank page for Taking an Exam. We then mob programed again to create a button for taking an exam in the manage exam page, then provided the navigation route towards take an exam page. Afterwards we continued to mob program and together created make a question table and connecting to the question endpoint data.

Jan 30, 2024
Sam and I partnered programmed, I drove the initial unit testing for get all exams. However we experienced some issues wit the user authentication. We troubleshooted the issue by calling in the dependency authentication that was shown on learn and created test data for a user to solve the issue. We decided to only do get an exam by id, made fake data and called on the api endpoint for exam id to show in the response and were able to pass one test so far.

Jan 31, 2024
I partnered with Denver today and created the hero page in our app, we discussed ways to later design this page. This is essentially our home page, that may later provide information how our web app works and some information about the developers to be designed at a later date. I also removed the take exam navigation from the drop down since we incorporated the take exam button when exploring the exams.

Feb 1, 2024
Today I partnered with Jeremy, I drove and we worked on adding CSS features to the front end and writing an introduction for users when coming to our website.

Feb 5, 2024
Today we worked together as a team to deploy. We all took turns implementing part of the deployment. I built the api-image and all dependencies that passed the pipeline test to main. This showed the image in the container repository on gitlab. When we were installing the CLI, I tried along with Jeremy, another mac user, to fetch the token. However, we both experienced issues with our security settings in that mac did not seem to render the certificate or token since it was generated from a third party that could contain malware, even after following the troubleshooting guides we experienced this blocker. A Windows user, Sam, managed to run the command as an admin and was able to get over that blocker.We continued to work on the deployment, and when running the front end we had a blocker error showing 503 temp unavailable. We decided to pick this up tomorrow.

Feb 6, 2024
Today we worked together to continue working on building and deploying our web application.  We finished adding the build front end job. We originally had an issue with the API URL and API host. We realized we needed to include these in our ghi env and updated all localhost:8000 to API_HOST. We also realized we had an extra back slash / in our API host and Public URL that was causing rendering issues when trying to open the web app after deployment. I drove the deployment by incorporating pages script to deploy the react front end to git lab. We managed to pass all the pipeline test. However, when continued to have some issues with our URL and CORS issues, also our pictures on the deployment weren’t showing whereas on our local it was showing. Several SIERS helped walk us through a few of the CORS issues and gave ideas how to resolve our image issues. In addition, while we were waiting for the SIERS I drove the creation of the README and we put together the users stories and outlined several features.

Feb 7, 2023
Today I partnered with Denver and I drove to add a feature that navigated get started button on hero page towards the sign up page if not logged in and to explore all exams if user is currently logged in.

Feb 8, 20230
Today we all worked putting the readme together. I drove the onboarding, unit tests, project tracking, and incorporating the wire frame designs.

1-16-24
Worked on Authentication with team

1-17-24
Finished up Authentication with the team and started working on the questions api endpoints

1-18-24
Finished up questions api end points

1-19-24
Created get_exams_by_creator api endpoint and helped the Crustacean team with the rest of the exam endpoints

1-22-24
Finished up our api endpoints

1-23-24

1-24-24
Worked on getting the login page up and running

1-25-24
We finished up the login page and got the logout functionality working. Sam and I built the grade book.jsx and linked it to the manage.jsx.

1-26-24
Worked with Jim in pair programming creating the attempted exam by user page and we got it partially working. Had a meeting with career services, went well.

1-29-24
Worked mob style with the group on finishing up the attempted exam by user page and then creating a take exam page and started with creating questions page.

1-30-24
Worked in mob style finishing up creating questions for exam. Worked with Denver on setting up a button that will create a question and submit the test.

1-31-24
Created a test to get exam by attempt id. We installed black and flake8 into our project and cleaned up our code. worked with Sam on changing get_exams_by_creator to pull creator_id from the input, not from the SQL statement.

2-1-24
Partnered with Jim on creating the heroPage and updating some tailwind CSS and about sections.

2-5-24
Started the CI/CD. We had a few issues and got almost finished with deployment. Will try to finish tomorrow.

2-6-24
Worked on finishing CI/CD, had a few hangups with deployment and our images not appearing upon deployment. We also started the README as well in between.

2-7-24
Cleaned up code in our app today. I took out all timestamps, datetime references and all console.logs. Also applied a filter method so that you wouldn't see any exams that did not have any questions applied to them.

2-8-24
Worked on the Database schema and linked it into the README. Then we finished up the README and made it nice looking.

2-9-24
Demo'd our project to Riley, then studied for the test the rest of the day.

﻿-- Exported from QuickDBD: https://www.quickdatabasediagrams.com/
-- NOTE! If you have used non-SQL datatypes in your design, you will have to change these here.

-- Modify this code to update the DB schema diagram.
-- To reset the sample schema, replace everything with
-- two dots ('..' - without quotes).

CREATE TABLE "Users" (
    "UserID" int   NOT NULL,
    "FirstName" string   NOT NULL,
    "LastName" string   NOT NULL,
    "Username" string   NOT NULL,
    "Email" string   NOT NULL,
    "Created" datetime   NOT NULL,
    CONSTRAINT "pk_Users" PRIMARY KEY (
        "UserID"
    )
);

CREATE TABLE "Exams" (
    "ExamID" int   NOT NULL,
    "ExamName" varchar(200)   NOT NULL,
    "CreaterID" int   NOT NULL,
    "Created" datetime   NOT NULL,
    CONSTRAINT "pk_Exams" PRIMARY KEY (
        "ExamID"
    )
);

-- Enrollment as C
-- -
-- StudentID int FK >- Students.StudentID
-- CourseID int FK >- Courses.CourseID
CREATE TABLE "ExamAttempts" (
    "AttemptID" int   NOT NULL,
    "UserID" int   NOT NULL,
    "ExamID" int   NOT NULL,
    "Score" Int   NOT NULL,
    "Created" datetime   NOT NULL,
    CONSTRAINT "pk_ExamAttempts" PRIMARY KEY (
        "AttemptID"
    )
);

CREATE TABLE "Responses" (
    "ResponseID" int   NOT NULL,
    "AttemptID" int   NOT NULL,
    "QuestionID" int   NOT NULL,
    "OptionID" int   NOT NULL,
    "CorrectBool" boolean   NOT NULL,
    "Created" datetime   NOT NULL,
    CONSTRAINT "pk_Responses" PRIMARY KEY (
        "ResponseID"
    )
);

CREATE TABLE "Questions" (
    "QuestionsID" int   NOT NULL,
    "ExamID" int   NOT NULL,
    "QuestionText" string   NOT NULL,
    "Created" datetime   NOT NULL
);

CREATE TABLE "Options" (
    "OptionID" int   NOT NULL,
    "QuestionID" int   NOT NULL,
    "OptionText" int   NOT NULL,
    "CorrectBool" string   NOT NULL,
    "Created" datetime   NOT NULL,
    CONSTRAINT "pk_Options" PRIMARY KEY (
        "OptionID"
    )
);

ALTER TABLE "Exams" ADD CONSTRAINT "fk_Exams_CreaterID" FOREIGN KEY("CreaterID")
REFERENCES "Users" ("UserID");

ALTER TABLE "ExamAttempts" ADD CONSTRAINT "fk_ExamAttempts_UserID" FOREIGN KEY("UserID")
REFERENCES "Users" ("UserID");

ALTER TABLE "ExamAttempts" ADD CONSTRAINT "fk_ExamAttempts_ExamID" FOREIGN KEY("ExamID")
REFERENCES "Exams" ("ExamID");

ALTER TABLE "Responses" ADD CONSTRAINT "fk_Responses_AttemptID" FOREIGN KEY("AttemptID")
REFERENCES "ExamAttempts" ("AttemptID");

ALTER TABLE "Responses" ADD CONSTRAINT "fk_Responses_QuestionID" FOREIGN KEY("QuestionID")
REFERENCES "Questions" ("QuestionsID");

ALTER TABLE "Responses" ADD CONSTRAINT "fk_Responses_OptionID" FOREIGN KEY("OptionID")
REFERENCES "Options" ("OptionID");

ALTER TABLE "Questions" ADD CONSTRAINT "fk_Questions_ExamID" FOREIGN KEY("ExamID")
REFERENCES "Exams" ("ExamID");

ALTER TABLE "Options" ADD CONSTRAINT "fk_Options_QuestionID" FOREIGN KEY("QuestionID")
REFERENCES "Questions" ("QuestionsID");

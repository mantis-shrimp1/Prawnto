from pydantic import BaseModel
from queries.pool import pool


class QuestionIn(BaseModel):
    question_text: str


class QuestionOut(BaseModel):
    question_id: int
    exam_id: int
    question_text: str


class QuestionOutList(BaseModel):
    questions: list[QuestionOut]


class QuestionQueries:
    def create(self, info: QuestionIn, exam_id: int) -> QuestionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO Questions (
                        exam_id,
                        question_text
                        )
                        VALUES ( %s, %s)
                        RETURNING question_id
                    """,
                    [exam_id, info.question_text],
                )
                id = result.fetchone()[0]
                old_data = info.dict()
                return QuestionOut(question_id=id, exam_id=exam_id, **old_data)

    def get_question_by_exam(self, exam_id: int) -> QuestionOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT (question_text, question_id)
                    FROM Questions
                    WHERE exam_id = %s;
                    """,
                    [exam_id],
                )
                records = result.fetchall()
                question_out_list = []
                for record in records:
                    new_questionOut = QuestionOut(
                        question_text=record[0][0],
                        question_id=record[0][1],
                        exam_id=exam_id,
                    )
                    question_out_list.append(new_questionOut)
                return QuestionOutList(questions=question_out_list)

    def get_question_count_by_exam(self, exam_id: int) -> int:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT COUNT(*)
                    FROM Questions
                    WHERE exam_id = %s;
                    """,
                    [exam_id],
                )
                record = result.fetchone()[0]
                return record

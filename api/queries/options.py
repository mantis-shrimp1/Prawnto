from pydantic import BaseModel
from queries.pool import pool


class OptionIn(BaseModel):
    option_text: str
    correct_bool: bool


class OptionOut(BaseModel):
    question_id: int
    option_text: str
    correct_bool: bool
    option_id: int


class OptionOutList(BaseModel):
    options: list[OptionOut]


class OptionQueries:
    def create(self, info: OptionIn, question_id: int) -> OptionOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        INSERT INTO Options (
                            question_id,
                            option_text,
                            correct
                        )
                        VALUES (
                        %s,
                        %s,
                        %s
                        )
                        RETURNING option_id
                    """,
                    [question_id, info.option_text, info.correct_bool],
                )
                id = result.fetchone()[0]
                old_data = info.dict()
                return OptionOut(
                    option_id=id, question_id=question_id, **old_data
                )

    def get_options_by_question_id(self, question_id: int) -> OptionOutList:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT (
                        option_id,
                        option_text,
                        correct
                    )
                    FROM Options WHERE question_id = %s;
                    """,
                    [question_id],
                )
                records = result.fetchall()
                option_out_list = []
                for record in records:
                    new_optionOut = OptionOut(
                        option_id=record[0][0],
                        question_id=question_id,
                        option_text=record[0][1],
                        correct_bool=record[0][2],
                    )
                    option_out_list.append(new_optionOut)
                return OptionOutList(options=option_out_list)

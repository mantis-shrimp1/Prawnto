from fastapi import Depends, APIRouter
from authenticator import authenticator
from queries.options import (
    OptionIn,
    OptionOut,
    OptionQueries,
)


router = APIRouter()


@router.post("/api/options/{question_id}")
async def create_question_option(
    info: OptionIn,
    question_id: int,
    option_queries: OptionQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> OptionOut:
    option = option_queries.create(info, question_id)
    return option


@router.get("/api/options/{question_id}")
async def get_options_by_question_id(
    question_id: int,
    option_queries: OptionQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    option = option_queries.get_options_by_question_id(question_id)
    return option

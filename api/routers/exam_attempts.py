from fastapi import Depends, APIRouter
from authenticator import authenticator
from queries.exam_attempts import (
    ExamAttemptIn,
    ExamAttemptOut,
    ExamAttemptQueries,
)

router = APIRouter()


@router.post("/api/exam_attempts/{exam_id}")
async def create_exam_attempt(
    info: ExamAttemptIn,
    exam_id: int,
    exam_attempt_queries: ExamAttemptQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ExamAttemptOut:
    exam_attempt = exam_attempt_queries.create(info, exam_id)
    return exam_attempt


@router.get("/api/exam_attempts/{exam_id}")
async def get_exams_attempts_by_exam_id(
    exam_id: int,
    exam_attempt_queries: ExamAttemptQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam_attempts = exam_attempt_queries.get_exams_attempts_by_exam_id(exam_id)
    return exam_attempts


@router.get("/api/exam_attempts/user/{user_id}")
async def get_exam_attempts_by_user_id(
    user_id: int,
    exam_attempt_queries: ExamAttemptQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam_attempts = exam_attempt_queries.get_exam_attempts_by_user_id(user_id)
    return exam_attempts


@router.get("/api/exam_attempts/exam/{attempt_id}")
async def get_exams_by_attempt_id(
    attempt_id: int,
    exam_attempt_queries: ExamAttemptQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    exam = exam_attempt_queries.get_exams_by_attempt_id(attempt_id)
    return exam


@router.get("/api/exam_attempts/{attempt_id}/grade")
async def grade_exam_by_attempt_id(
    attempt_id: int,
    exam_attempt_queries: ExamAttemptQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    grade = exam_attempt_queries.grade_by_attempt_id(attempt_id)
    return grade

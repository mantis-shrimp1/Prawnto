from fastapi import Depends, APIRouter
from authenticator import authenticator
from queries.responses import ResponseIn, ResponseOut, ResponseQueries

router = APIRouter()


@router.post("/api/responses/{attempt_id}")
async def create_question_responses(
    info: ResponseIn,
    attempt_id: int,
    response_queries: ResponseQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> ResponseOut:
    responses = response_queries.create(info, attempt_id)
    return responses

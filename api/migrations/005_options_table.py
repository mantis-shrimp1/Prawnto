steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE Options (
            option_id SERIAL PRIMARY KEY,
            question_id INT REFERENCES Questions(question_id),
            option_text VARCHAR(500) NOT NULL,
            correct BOOLEAN NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE Options;
        """,
    ]
]
